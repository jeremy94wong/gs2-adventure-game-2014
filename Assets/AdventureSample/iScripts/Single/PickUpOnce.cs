using UnityEngine;
using System.Collections;

public class PickUpOnce : BasePickUp
{
	protected override void OnSuccess()
	{
		base.OnSuccess();
		
		Interactable.GetInteraction(this).AvailableInScene = false;
	}
	
	protected override void OnOutOfRange()
	{
	}
}

