using UnityEngine;
using System;
using System.Collections;

/// <summary>
/// Utility class containing string-related helpers.
/// </summary>
public static class StringUtility
{	
	/// <summary>
	/// Converts string in camel-case format to a more human-readable format;
	/// </summary>
	public static string CamelToHuman(string camel)
	{
		if (camel.Length < 1)
			return camel;
		
		char[] humanArray = camel.ToCharArray();
		
		if (Char.IsLower(humanArray[0]))
			humanArray[0] = Char.ToUpper(humanArray[0]);
		
		string human = new string(humanArray);
		
		for (int i = 1; i < human.Length; i++)
		{
			if (Char.IsUpper(human[i]))
			{
				human = human.Insert(i, " ");
				i++;	// skip over
			}
			else if (!Char.IsLetter(human[i]))
			{
				human = human.Insert(i, " ");
				i++;	// skip over
			}
		}
		
		return human;
	}
	
	/// <summary>
	/// Returns whether a char is a vowel.
	/// </summary>
	public static bool IsVowel(char letter)
	{
		if (char.IsUpper(letter))
			letter = Char.ToLower(letter);
		
	    return letter == 'e' ||
		letter == 'a' ||
		letter == 'o' ||
		letter == 'i' ||
		letter == 'u';
	}
}
